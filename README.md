# Setup

## First setup

The different repositories you will clone should respect some organization
in order for the different tools, like `rcd` and `scd` to work.

The repositories are stored in the `${WORK}` directory (currently set to
`$HOME/work` in `.zshr` and organized by the `origin` remote by host and path.

Use `git_clone` to clone a repository at the correct location. When cloning
this repository for the first time you will not have the tool so clone it
manually at the correct place.

The commands to setup the configurations

``` bash
setup_dir=${HOME}/work/gitlab/hpaluche
mkdir -p "${setup_dir}"
git clone git@gitlab.com:hpaluche/setup.git "${setup_dir}"
cd ${setup_dir}
```

Run `./install.sh`.

## Custom configuration

### Local zshrc

The .zshrc contains the configuration shared across my computers. Each has a
custom configuration, the place to put it is in
`${HOME}/.config/zsh/local_zshrc`

## nvim

Run `:PlugInstall` in nvim to install the modules.

### CoC
CoC needs node.js with a version later than the one provided by Ubuntu 22.04.

Installation instructions are here:
https://github.com/nodesource/distributions#installation-instructions

Install `coc-rust-analyzer` by running `:CocInstall coc-rust-analyzer` once
node.js is correctly installed.

### Bluetooth

blueman-applet is shit. Use `bluetui`

``` bash
cargo install bluetui
```

Press `?` for help.
