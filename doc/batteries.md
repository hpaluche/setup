# Batteries status

## Bluetooth devices

In order to obtain the battery status of a bluetooth device, you need to set
the bluetooth configuration `/etc/bluetooth/main.conf` the line

```
Experimental = true
```

to the `[General]` section.
