# Keychron  K6


## udev

  Extract of documentation fron https://github.com/kurgol/keychron

This udev rule is credited to [Emilio Coppa](https://www.facebook.com/ercoppa) from the [Facebook: Keychron User Group](https://www.facebook.com/profile.php?id=534114427066453&ref=br_rs) and was provided as [GitHub Gist: ercoppa/80-keychron.rules](https://gist.github.com/ercoppa/87a42a5d1fd65539844d7badc276d8e7).

1. Create udev rule `sudo touch /etc/udev/rules.d/80-keychron.rules`
2. Add the udev rule `SUBSYSTEMS=="input", ATTRS{name}=="Keychron K6", RUN+="echo 0 | tee /sys/module/hid_apple/parameters/fnmode"` to `/etc/udev/rules.d/80-keychron.rules`
3. `sudo udevadm control --reload-rules && sudo udevadm trigger`

An alternative solution was offered by [Thạch Nguyễn](https://www.facebook.com/Cobblestone8x).

1. Set fkeyfirst by:
  - create the conf file `sudo touch /etc/modprobe.d/hid_apple.conf`.
  - add this line to the file: `options hid_apple fnmode=1` and save
  - `sudo update-initramfs -u`
  - `reboot`
2. Use the keyboard in Windows/Android mode
