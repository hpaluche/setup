# QWERTY-FR Keyoard Layout

## Installation
https://qwerty-fr.org/

## Setting this as default layout

Using dconf-editor

```sh
sudo apt-get install dconf-editor

dconf-editor
```

- Go to `/org/gnome/desktop/input-sources/sources`
- Unset `use default value`
- Set `Custom value` to `[('xkb', 'us+qwerty-fr')]`
