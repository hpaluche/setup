# shellcheck shell=bash
# File meant to be sourced.
# shellcheck disable=SC2034
GIT_ROOT=$(pwd)
PRE_COMMIT_CONFIG=".pre-commit-config.yaml"
HOOK_TYPE=$(basename "${0}")
HOOK_DIR=$(realpath "$(dirname "${0}")")
