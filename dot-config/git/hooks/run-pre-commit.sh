# shellcheck shell=bash
pre_commit_res=0

if [ -e "${GIT_ROOT}/${PRE_COMMIT_CONFIG}" ]
then
    # This script is meant to be sourced
    # shellcheck disable=SC2034
    /usr/bin/env python3                \
        -m pre_commit                   \
        hook-impl                       \
        --config="${PRE_COMMIT_CONFIG}" \
        --hook-type="${HOOK_TYPE}"      \
        --hook-dir="${HOOK_DIR}"        \
        --                              \
        "$@" || pre_commit_res=$?
fi # pre-commit
