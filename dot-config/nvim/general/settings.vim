" set leader key {{{
  let g:mapleader = ","
" }}}

" Colors {{{
set t_Co=256         " Support 256 colors
set background=dark  " tell vim what the background color looks like
" }}}

" Misc  {{{
syntax enable                           " Enables syntax highlighing
set encoding=utf-8                      " The encoding displayed
set hidden                              " Required to keep multiple buffers open multiple buffers
set fileencoding=utf-8                  " The encoding written to file
set lazyredraw                          " prevent redrawing during macro
" }}}

" Undo settings {{{
set undodir=~/.config/nvim/undo
set undolevels=10000
set undoreload=10000
" }}}

" Spaces & Tabs {{{
set tabstop=8       " 1 indentation level = 8 spaces
set shiftwidth=0    " Change the number of space characters inserted for indentation
set shiftround
set expandtab       " Ident using spaces, not tabs
set smartindent
set textwidth=79
set smarttab        " Makes tabbing smarter will realize you have 2 vs 4
set smartindent     " Makes indenting smart
set autoindent      " Good auto indent
" }}}

" UI Layout {{{
set nowrap          " Turn off line wrap
set pumheight=10    " Makes popup menu smaller
set ruler           " Show the cursor position all the time
set mouse=a         " Enable your mouse
set title
set cursorline      " Enable highlighting of the current line
set splitbelow      " Horizontal splits will automatically be below
set splitright      " Vertical splits will automatically be to the right
set number          " turn on numbered lines
set relativenumber  " turn on after number
set showtabline=0   " Always show tabs
set wildmenu        " visual autocomplete for command menu
set showmatch       " highlight matching {[()]}
set matchtime=10
set laststatus=2    " always show the status line
" }}}

" Searching {{{
noremap / /\v
set ignorecase
set smartcase
" }}}

" Spellchecker ------------------------------------------------------------ {{{
set spell spelllang=en_us
" }}}

set inccommand=split
set wildmode=longest,list,full
set completeopt=menuone,noinsert,noselect
set foldmethod=marker

set iskeyword-=-   " dash separated words as a word text object"
set conceallevel=0        " So that I can see `` in markdown files
set updatetime=300        " Faster completion
set timeoutlen=500        " By default timeoutlen is 1000 ms
set clipboard=unnamedplus " Copy paste between vim and everything else
let g:vimsyn_embed = 'lPr'
set scrolloff=3

" ## Program enhancements
set nomodeline
set pyxversion=3
let g:loaded_python_provider = 0


" Automation {{{
" au! BufWinEnter * let w:m1=matchadd('ErrorMsg', '\%>100v.\+', -1)
au! BufWritePost *.vim source % " auto source when writing to init.vm alternatively you can run :source $MYVIMRC

function FileClean()
    let save_cursor = getpos('.')
    " Remove any empty line at the end of the file, except one.
    silent! %s#\($\n\s*\)\+\%$##
    " Remove any trailing whitespace
    silent! %s#\s\+$##
    call setpos('.', save_cursor)
endfunction

autocmd BufWritePre * call FileClean()

" In the case of git diff, context line being empty should have 1 trailing
" whitespace.
function FileCleanDiff()
    let save_cursor = getpos('.')
    silent: %s/^$/ /e
    call setpos('.', save_cursor)
endfunction

autocmd BufWritePre *.diff call FileCleanDiff()
" }}}

set showcmd              " count highlighted
set colorcolumn=80       " vertical ruler at 80 character
set textwidth=79         " Automatic word wrapping at 80 character
set noshowmode           " We don't need to see things like -- INSERT -- anymore airline does it for us

" List chars to highlight {{{
set listchars=           " Clear list
set listchars=nbsp:.     " Add non breaking space to list
set listchars=tab:>-     " Add tab to list
set list                 " Activate list
" }}}

function TabOnPurpose()
set tabstop=8
set listchars=tab:\ \  " make tabs invisible
endfunction

command TabOnPurpose call TabOnPurpose()

set vb                   " Don't beep at me
set backspace=2          " Backspace over indent, eol, and insert

" mouse {{{
set mousehide  " Hide the mouse pointer while typing
set mouse=nc   " Activate mouse support in all mode except insertion and visual
" }}}
