" # init.vim
"
scriptencoding utf-8
source $HOME/.config/nvim/general/settings.vim

source $HOME/.config/nvim/vim-plug/plugins.vim

source $HOME/.config/nvim/plug-config/ale.vim
source $HOME/.config/nvim/plug-config/telescope.vim
source $HOME/.config/nvim/plug-config/coc.vim
source $HOME/.config/nvim/plug-config/start-screen.vim
source $HOME/.config/nvim/plug-config/align.lua

source $HOME/.config/nvim/themes/airline.vim
source $HOME/.config/nvim/themes/gruvbox.vim

source $HOME/.config/nvim/keys/mappings.vim
