" Better nav for omnicomplete
" inoremap <expr> <c-j> ("\<C-n>")
" inoremap <expr> <c-TAB> ("\<C-n>")
" inoremap <expr> <c-k> ("\<C-p>")

" Use alt + hjkl to resize windows
    " alt h = ˙
    " alt j = ∆
    " alt k = ˚
    " alt l = ¬
"nnoremap ˚   :resize -10<CR>
"nnoremap ∆   :resize +10<CR>
"nnoremap ¬   :vertical resize -10<CR>
"nnoremap ˙   :vertical resize +10<CR>

" I hate escape more than anything else
" inoremap jk <Esc>
" inoremap jj <Esc>
" inoremap kk <Esc>

" easynotion mapping

"look for 1 char input in the line
"  nmap s <Plug>(easymotion-sl)
"look for 2 char input in all windows
"  nmap f <Plug>(easymotion-overwin-f2)
"use space to jump to the first result
"  let g:EasyMotion_space_jump_first = 1
"  map <Leader>h <Plug>(easymotion-linebackward)
"  map <Leader>l <Plug>(easymotion-lineforward)
"  map <Leader>j <Plug>(easymotion-j)
"  map <Leader>k <Plug>(easymotion-k)
"  map <Leader>J <Plug>(easymotion-eol-j)
"  map <Leader>K <Plug>(easymotion-eol-K)


" Easy CAPS
"inoremap <c-u> <ESC>viwUi
" nnoremap <c-u> viwU<Esc>
"
" Ctrl + TAB in general mode will move to text buffer
"nnoremap <TAB> :bnext<CR>
" SHIFT-TAB will go back
"nnoremap <S-TAB> :bprevious<CR>

" Ctrl + x  ||   close current buffer
"nnoremap <C-x> :bd<CR>

" Alternate way to save
"nnoremap <C-s> :w<CR>
" Alternate way to quit
"nnoremap <C-Q> :wq!<CR>
" Use control-c instead of escape
"nnoremap <C-c> <Esc>
" <TAB>: completion.
" inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

" Better tabbing
vnoremap < <gv
vnoremap > >gv

" un-highlight search result
noremap <leader><space> :noh<CR>

" Open explore
noremap <leader>e :Vexplore<CR>

" Shortcut to rapidly toggle `set list`
nnoremap <leader>l :set list!<CR>

" Better window navigation
" nnoremap <C-left>  <C-w>h
" nnoremap <C-right> <C-w>j
" nnoremap <C-up>    <C-w>k
" nnoremap <C-right> <C-w>l
" nnoremap <C-h>     <C-w>h
" nnoremap <C-j>     <C-w>j
" nnoremap <C-k>     <C-w>k
" nnoremap <C-l>     <C-w>l

" trying to fix copy and page
" nnoremap s "a

" Be nice with my fingers
cnoreabbrev <expr> W ((getcmdtype() is# ':' && getcmdline() is# 'W')?('w'):('W'))
cnoreabbrev <expr> Wq ((getcmdtype() is# ':' && getcmdline() is# 'Wq')?('wq'):('Wq'))
cnoreabbrev <expr> WQ ((getcmdtype() is# ':' && getcmdline() is# 'WQ')?('wq'):('WQ'))
cnoreabbrev <expr> Q ((getcmdtype() is# ':' && getcmdline() is# 'Q')?('q'):('Q'))

" Normal/Visual tab for bracket pairs
noremap <tab> %
vnoremap <tab> %

" ALE shortcuts
noremap <leader>n :ALENext<CR>

" un-highlight search result
noremap <leader><space> :noh<CR>

" Split a line based on the commas.
noremap <leader><leader> :.s/,/,\r/g<CR>

noremap <leader>p :ALEPrevious<CR>

autocmd FileType rust nnoremap <leader>f :ALEFix rustfmt<CR>
