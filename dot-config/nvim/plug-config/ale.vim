let g:ale_rust_cargo_use_clippy = executable('cargo-clippy')
let g:ale_echo_msg_format = '[%linter%:%type%%:code%] %s '
let g:ale_keep_list_window_open = 1
let b:ale_linters = {'c':'gcc'}
