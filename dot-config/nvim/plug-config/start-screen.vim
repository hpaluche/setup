" :SLoad       load a session
"# :SSave[!]    save a session
"# :SDelete[!]  delete a session
"# :SClose      close a session

let g:startify_session_dir = '~/.config/nvim/session'

let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Files']            },
          \ { 'type': 'dir',       'header': ['   Current Directory '. getcwd()] },
          \ { 'type': 'sessions',  'header': ['   Sessions']       },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ ]


let g:startify_bookmarks = [
            \ { 'c': '~/.config/' },
            \ { 'a': '~/.config/awesome/rc.lua' },
            \ { 'Z': '~/.zshrc' },
            \ { 'z': '~/.config/zsh/local_zshrc' },
            \ { 'n': '~/.config/nvim' },
            \ ]



let g:startify_session_autoload = 1
let g:startify_session_delete_buffers = 1

let g:startify_custom_header = [
       \ '                                         __         ',
       \ '                                         /          ',
       \ '                                       Y.           ',
       \ '                              _______    `.         ',
       \ '              ,-------------"======="--""""-""""---.',
       \ '         __,=+"-------------------------------------|',
       \ '      .-/__|_]_]  :"/:""""""""""""""""""""""""""""""|',
       \ '   ,-"__________[];/_;______________________________|',
       \ ' ,".../_|___________________________________________|',
       \ '(_>        ,-------.                     ,-------.  |',
       \ ' `-._____."(_)`="(_)\_7___7___7___7__7_."(_)`="(_)\_/']
