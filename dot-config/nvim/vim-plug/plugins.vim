" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif


call plug#begin('~/.config/nvim/autoload/plugged')
    Plug 'sheerun/vim-polyglot'                   " Better Syntax Support
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'airblade/vim-gitgutter'
    Plug 'tpope/vim-fugitive'
    Plug 'heavenshell/vim-pydocstring', { 'do': 'make install' }
    Plug 'godlygeek/tabular'
    Plug 'vim-scripts/DoxygenToolkit.vim'
    Plug 'rust-lang/rust.vim'
    Plug 'dense-analysis/ale'
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'mhinz/vim-startify'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'Vonr/align.nvim',
call plug#end()


" TypeScript yats config
let g:yats_host_keyword = 1
set re=0
