#!/usr/bin/env python3

# pylint:disable=invalid-name

"""Script to list current branch in the prompt."""

import os
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from pygit2 import Repository
from paluche.logging import print_format, Color


def __main__():
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
                            description=__doc__)

    parser.parse_args()

    repository = Repository(os.getcwd())

    if repository.head_is_unborn:
        shorthand = repository.references['HEAD'].target[len('refs/heads/'):]
        print_format(f'{shorthand}|no commits', fg=Color.RED)
        return

    head = repository.head

    if head.shorthand != 'HEAD':
        print_format(head.shorthand, fg=Color.BLUE)
        return

    ret = ''

    for branch_name in repository.branches.local:
        if head.target != repository.branches[branch_name].target:
            continue

        if not ret:
            ret = '  '
        else:
            ret = ', '

        ret += branch_name

    print_format('DETACHED' + ret, fg=Color.YELLOW, italic=True)


# Main entry point.
if __name__ == '__main__':
    __main__()
