#!/usr/bin/env python3
# pylint:disable=invalid-name

"""This script will and update in a lightweight way a SVN repository.

The idea is to clone everything every location which is not a trunk directory,
branches or tags sub-directory with `--set-depth immediates`. The code for the
"references" are to be updated with `--set-depth infinite` when needed.

"""

import os
from argparse import ArgumentParser
from subprocess import check_call
from paluche.repo import parse_remote_url, load_remote_hosts
from rich.console import Console
from rich.syntax import Syntax


def exec_cmd(cmd, console):
    """Pretty print the command and execute it"""
    cmd_line = [
        arg if ' ' not in arg else f'"{arg}"'
        for arg in cmd
    ]
    syntax = Syntax(
        ' '.join(cmd_line),
        'bash',
        theme='monokai',
        line_numbers=False,
    )
    console.print(syntax)

    check_call(cmd)


def svn_checkout(url, path, console):
    """Checkout repository."""
    exec_cmd(
        [
            'svn',
            'checkout',
            '--depth', 'immediates',
            url,
            path
        ],
        console
    )


def svn_update(path, set_depth, console):
    """Update a sub-directory of the repository."""
    exec_cmd(
        [
            'svn',
            'update',
            '--set-depth', set_depth,
            path
        ],
        console
    )


def main():
    """Main function"""
    parser = ArgumentParser(description=__doc__)

    parser.add_argument('url', help='URL of the SVN repository to checkout')

    args = parser.parse_args()

    path = parse_remote_url(
        args.url,
        load_remote_hosts()
    )['location']
    dir_path = os.path.dirname(path)
    console = Console()

    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    if os.path.exists(path):
        parser.error(f'{path} already exists')

    svn_checkout(args.url, path, console)

    for dir_path, dir_names, file_names in os.walk(path):
        if os.path.basename(dir_path) in ('tags', 'branches'):
            # Do not go any deeper.
            dir_names.clear()
            continue

        if '.svn' in dir_names:
            # Do no touch .svn directory
            del dir_names[dir_names.index('.svn')]

        if 'trunk' in dir_names:
            svn_update(os.path.join(dir_path, 'trunk'), 'infinity', console)
            del dir_names[dir_names.index('trunk')]

        for dir_name in dir_names:
            svn_update(os.path.join(dir_path, dir_name), 'immediates', console)

        for file_name in file_names:
            svn_update(os.path.join(dir_path, file_name), 'infinity', console)


if __name__ == '__main__':
    main()
