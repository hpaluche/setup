#! /usr/bin/env bash

set -e

THIS_SCRIPT=$(readlink -f "${0}")
SETUP_DIR="${THIS_SCRIPT%/*}"
RESOURCES_DIR="${SETUP_DIR}/resources"
UDEV_DIR="${SETUP_DIR}/udev"

function install_package()
{
  package_name=$1

  sudo apt-get install -y "${package_name}"

}

PACKAGES="\
  awesome
  neovim
  zsh
  kitty
  shellcheck
  python3-pip
  stow
  bat
  silversearcher-ag
  fzf
"

"${SETUP_DIR}/submodule_update.sh" update

sudo apt-get update -y
sudo apt-get dist-upgrade -y

for package in ${PACKAGES}
do
  install_package "${package}"
done

python3 -m pip install -r "${SETUP_DIR}/python/requirements.txt"

{
  cd "${SETUP_DIR}" || exit
  pre-commit install
}

# Install the qwerty-fr keyboard layout.
sudo dpkg --install "${RESOURCES_DIR}/qwerty-fr_0.7.2_linux.deb"

# Install udev rules
UDEV_DST=/etc/udev/rules.d
RULES=$(find "${UDEV_DIR}" -name "*.rules" -printf "%f ")
for file in ${RULES}
do
  sudo cp "${UDEV_DIR}/${file}" "${UDEV_DST}/${file}"
  sudo chown root:root "${UDEV_DST}/${file}"
done
sudo udevadm control --reload-rules && sudo udevadm trigger

# Install the SauceCodePro Nerd Font.
FONTS_DIR="${HOME}/.fonts"
mkdir -p "${FONTS_DIR}"
unzip "${RESOURCES_DIR}/SourceCodePro.zip" -qq -d "${FONTS_DIR}"

# Reload cache fonts
fc-cache -fv

# Install the configuration files as symlink using stow.
"${SETUP_DIR}/stow_install.sh"

# Install a crontab to periodically update apt cache for
cat >> /etc/crontab << EOF
16 *	* * *	root	apt update
EOF
