"""Utils regarding git version control system"""

import os
import sys
from configparser import ConfigParser
from pygit2 import Repository
from paluche.repo import (
    RepoType, parse_remote_url, get_parsed_url, get_repo_root,
)

DEFAULT_BRANCH_NAMES = ('master', 'main')


def get_local_branch_name(remote_branch):
    """Get the local branch name associated with a remote branch.

    :param remote_branch: Remote branch you want the local name of.
    :type remote_branch: pygit2.Branch

    :raises ValueError: Provided branch is not a remote one.

    :return: Local branch name.
    :rtype: str.
    """
    return remote_branch.branch_name[len(f'{remote_branch.remote_name}/'):]


def get_remote_name(branch):
    """Get the remote name of a branch.

    :param branch: Branch you want the remote name of.
    :type branch: pygit2.Branch

    :return: The name of the remote the branch is associated to, returns None
             if the branch is a local one.
    :rtype: str, None
    """

    # As pygit2 is far from perfect the branch.type attribute always says that
    # it is a local branch. The only way I found is to attempt to access the
    # remote_name which, in case the branch is local, will raise a ValueError.
    try:
        # Try to access the remote name.
        return branch.remote_name
    except ValueError:
        # Accessing remote_name failed, branch is local one.
        return None


def is_branch_remote(branch):
    """Determine if the branch is a remote one.

    :param branch: Branch you want to know if it is a remote one.
    :type branch: pygit2.Branch

    :return: True if the branch is a remote one, False otherwise.
    :rtype: bool
    """
    return bool(get_remote_name(branch))


def is_rebased_on(repository, reference, target):
    """Find out if a specific reference is rebased on top of a specific target
    reference.

    :param repository: Repository the reference are in.
    :type repository: pygit2.Repository
    :param reference: Git reference you want to know if it is rebased on
                      target.
    :type reference: str
    :param target: Git reference you want to know if reference in rebased on.
    :type target: str

    :return: True if reference is rebased on target, False otherwise.
    :rtype: bool
    """
    return repository.merge_base(target, reference) == target


def load_repo_url(repo_root, remote_hosts):
    """Load the URL associated with the repository"""
    repo = Repository(repo_root)

    for remote in repo.remotes:
        if remote.name == 'origin':
            url = remote.url
            break
    else:
        try:
            url = repo.remotes[0].url
        except IndexError:
            url = None

    ret = get_parsed_url(url, repo_root, remote_hosts)

    if ret['location'] and repo_root != ret['location']:
        print(
            f'Unexpected location for the repository {ret["path"]}. Actually '
            f'in "{repo_root}" should be in "{ret["location"]}".',
            file=sys.stderr
        )

    return ret


def load_gitmodules(repo_root, remote_url, remote_hosts):
    """Load the .gitmodules file"""
    gitmodules_path = os.path.join(repo_root, '.gitmodules')

    if not os.path.isfile(os.path.join(gitmodules_path)):
        return {}

    gitmodules = ConfigParser()

    gitmodules.read(gitmodules_path)

    submodules = {}

    for section_id in gitmodules.sections():
        submodule_url = gitmodules[section_id]['url']
        submodule_path = os.path.join(repo_root,
                                      gitmodules[section_id]['path'])

        if submodule_url.startswith('..'):
            # Relative URL
            ret = remote_url.copy()
            ret['path'] = os.path.normpath(
                os.path.join(submodule_url.replace('/', os.sep), submodule_url)
            )
        else:
            ret = parse_remote_url(submodule_url, remote_hosts)

        ret['location'] = os.path.join(repo_root, submodule_path)
        ret['work_path'] = ret['location'][len(os.environ['WORK_DIR']) + 1:]
        ret['name'] = os.path.basename(ret['location'])

        submodules[ret['location']] = {'url': ret}

    return submodules


def load_current_repository(remote_hosts):
    """Load the information about the current repository and its submodule"""
    ret = {}

    # Search for the main repository (not a submodule) we could be in.
    if repo_root := get_repo_root(
        os.getcwd(),
        main_only=True,
        repo_type=RepoType.GIT
    ):
        # Load the main repository
        url = load_repo_url(repo_root, remote_hosts)

        ret = {url['location']: {'url': url}}

        # Load the submodules for that repository.
        ret.update(load_gitmodules(repo_root, ret[repo_root], remote_hosts))

    return ret
