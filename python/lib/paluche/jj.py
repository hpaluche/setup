"""Utils regarding jj version control system."""
import os
from subprocess import check_output
from paluche.repo import (
    RepoType, load_remote_hosts, get_parsed_url, search_for_repositories
)


def load_repo_url(repo_root, remote_hosts):
    ret = None
    cmd = ['jj', '--repository', repo_root, 'git', 'remote', 'list']
    for line in check_output(cmd, encoding='utf-8').splitlines():
        name, url = line.split(' ', maxsplit=1)
        if name == 'origin':
            ret = url
            break

        if ret is None:
            ret = url

    return get_parsed_url(ret, repo_root, remote_hosts)


def load_jj_repositories():
    """Load the jj repositories in the specified search directory"""
    remote_hosts = load_remote_hosts()

    return {
        url['location']: {'url': url}
        for url in (
            load_repo_url(repo_root, remote_hosts)
            for repo_root in search_for_repositories(
                os.environ['WORK_DIR'],
                repo_type=RepoType.JJ
            )
        )
    }
