"""Module to build logs and formatted output and overlay to print the logs from
the logging built module.
"""

from enum import IntEnum

#
# Global format option
#


# Boolean which make all the color and blinking format from the function below
# enabled or not.
__FORMAT_ENABLED = True

# Boolean which make all the blinking format from the function below enabled or
# not.
__BLINKING_ENABLED = True


#
# String output format utils
#


class Color(IntEnum):
    """The colors you can use in escape sequence formats."""
    BLACK = 0
    RED = 1
    GREEN = 2
    YELLOW = 3
    BLUE = 4
    MAGENTA = 5
    CYAN = 6
    WHITE = 7


class ANSIString():
    """Build a string with ANSI codes in it.

    Create a new string object from the given object. If encoding or
    errors is specified, then the object must expose a data buffer
    that will be decoded using the given encoding and error handler.
    Otherwise, returns the result of object.__str__() (if defined)
    or repr(object).

    Only the keyword arguments are documented:
    :param encoding: Encoding used to decode the provided objesct, defaults to
                     sys.getdefaultencoding().
    :type encoding: str
    :param errors: Control how encoding errors are handled. If 'strict' a
                   UnicodeError exception is raised. Other possible values are
                   'ignore', 'replace', 'xmlcharrefreplace', 'backslashreplace'
                   and any other name registered via codecs.register_error().
                   See Error Handlers for details.
    :type errors: str
    :param auto_reset: At the end of the colored output automatically resets
                       any formatting to the default one. Defaults to True.
    :type errors: bool
    """

    __ANSI_ESCAPE_START = '\033['
    __ANSI_ESCAPE_END = "m"

    def __init__(self, *args, **kwargs):
        self.__codes = []

        self.auto_reset = kwargs.pop('auto_reset', True)

        self.__raw = str(*args, **kwargs)

    def _push_code(self, code):
        self.__codes.append((len(self), code))

    def _push_color_code(self, color, bright, base):
        if not isinstance(color, Color):
            color = Color(color)

        self._push_code(color + base + (60 if bright else 0))

    def _push_format_code(self, code, enable):
        self._push_code(code + (20 if enable else 0))

    def reset(self):
        """Reset any color / format."""
        self._push_code(0)

    # pylint: disable-next=invalid-name,too-many-arguments
    def set(self, fg=None, fg_bright=False, bg=None, bg_bright=False,
            bold=None, faint=None, italic=None, underline=None, blink=None):
        """Escape sequence which will make a formatting coloring the text i
        printing after it. More information on the different formatting:
            https://en.wikipedia.org/wiki/ANSI_escape_code#Codes

        :param fg: Foreground color. Color of the text printed. If None default
                   foreground color from your terminal will be used. Defaults
                   to None.
        :param fg_bright: Boolean to indicate to the bright version for the
                          foreground color. Defaults to False.
        :param bg: Background color. If None default background color from your
                   terminal will be used. Defaults to None.
        :param bg_bright: Boolean to indicate to the bright version for the
                          background color. Defaults to False.
        :param bold: Boolean to indicate to the enable or disable the bold
                     format on the text. If None the bold format is left
                     untouched. If False it would also disable the faint
                     formatting. Defaults to None.
        :param faint: Boolean to indicate to the enable or disable the faint
                      format on the text. If None faint format is left
                      untouched. If False it would also disable the bold
                      formatting. Defaults to None.
        :param italic: Boolean to indicate to enable or disable the italic
                       format on the text. If None the italic format is left
                       untouched. Defaults to None.
        :param underline: Boolean to indicate to the enable or disable the
                          underline format on the text. If None underline
                          format is left untouched. Defaults to None.
        """

        if fg is not None:
            self._push_color_code(fg, fg_bright, 30)

        if bg is not None:
            self._push_color_code(bg, bg_bright, 40)

        if faint is not None:
            self._push_format_code(1, faint)

        if bold is not None:
            self._push_format_code(2, bold)

        if italic is not None:
            self._push_format_code(3, italic)

        if underline is not None:
            self._push_format_code(4, underline)

        if blink is not None:
            self._push_format_code(5, blink)

    def raw(self):
        """Get the raw string without any ANSI codes in it."""
        return self.__raw

    def __add__(self, value):
        self.__raw += value
        return self

    def __getitem__(self, key):
        return self.__raw.__getitem__(key)

    def __len__(self):
        return len(self.__raw)

    def __str__(self):
        if not self.__codes:
            return self.__raw

        ret = ''
        index = 0

        for code_index, code in self.__codes:
            if not ret:
                ret += self.__raw[index:code_index]
                ret += self.__ANSI_ESCAPE_START

            elif index != code_index:
                ret += self.__ANSI_ESCAPE_END
                ret += self.__raw[index:code_index]
                ret += self.__ANSI_ESCAPE_START
            else:
                ret += ';'

            ret += f'{code}'

            index = code_index

        # Finish the string.
        ret += self.__ANSI_ESCAPE_END
        ret += self.__raw[index:]

        # Reset colors
        if self.auto_reset:
            ret += f'{self.__ANSI_ESCAPE_START}0{self.__ANSI_ESCAPE_END}'

        return ret


# pylint: disable-next=invalid-name,too-many-arguments
def get_color_format(reset=True, fg=None, fg_bright=False, bg=None,
                     bg_bright=False, bold=None,  faint=None, italic=None,
                     underline=None):
    """Escape sequence which will make a formatting coloring the text printing
    after it. More information on the different formatting:
        https://en.wikipedia.org/wiki/ANSI_escape_code#Codes

    :param reset: Boolean to indicate to reset all previous format. Defaults to
                  True.
    :param fg: Foreground color. Color of the text printed. If None default
               foreground color from your terminal will be used. Defaults to
                None.
    :param fg_bright: Boolean to indicate to the bright version for the
                      foreground color. Defaults to False.
    :param bg: Background color. If None default background color from your
               terminal will be used. Defaults to None.
    :param bg_bright: Boolean to indicate to the bright version for the
                      background color. Defaults to False.
    :param bold: Boolean to indicate to the enable or disable the bold format
                 on the text. If None the bold format is left untouched. If
                 False it would also disable the faint formatting. Defaults to
                 None.
    :param faint: Boolean to indicate to the enable or disable the faint format
                  on the text. If None faint format is left untouched. If False
                  it would also disable the bold formatting. Defaults to None.
    :param italic: Boolean to indicate to enable or disable the italic format
                   on the text. If None the italic format is left untouched.
                   Defaults to None.
    :param underline: Boolean to indicate to the enable or disable the
                      underline format on the text. If None underline format is
                      left untouched. Defaults to None.
    :param bold: Boolean to indicate to the enable or disable the blinking
                 format on the text. If None returns no escape sequence.

    :return: Escape sequence which will produce the desired format on the text
             which follows it.
    """
    # pylint: disable-next=global-variable-not-assigned
    global __FORMAT_ENABLED

    def _set_color_code(codes, color_base, is_bright, code_modifiers):
        if color_base is None:
            return

        if color_base not in list(Color):
            raise ValueError()

        enable, disable = code_modifiers

        if is_bright:
            codes.append(enable + color_base)
        else:
            codes.append(disable + color_base)

    codes = []

    if not __FORMAT_ENABLED:
        return ''

    if reset:
        codes.append('0')

    _set_color_code(codes, fg, fg_bright, (90, 30))

    _set_color_code(codes, bg, bg_bright, (100, 40))

    if bold is not None or faint is not None:
        if bold:
            codes.append(1)
        elif faint:
            codes.append(2)
        else:
            codes.append(22)

    if italic is not None:
        if italic:
            codes.append(3)
        else:
            codes.append(23)

    if underline is not None:
        if underline:
            codes.append(4)
        else:
            codes.append(24)

    if codes:
        return '\033[' + ';'.join(f'{code}' for code in codes) + 'm'

    return ''


def get_blink_format(enable):
    """Escape sequence which will make a formatting blink the text printing
    after it. More information on the different formatting:
    https://en.wikipedia.org/wiki/ANSI_escape_code#Codes

    Rendering depends on the terminal used. From some testing on one terminal
    configuration the blinking does not work when background color is set.

    :param enable: Boolean to indicate to the enable or disable the blinking
                   format on the text. If None returns no escape sequence.

    :return: The escape sequence desired.
    """
    # pylint: disable-next=global-variable-not-assigned
    global __FORMAT_ENABLED
    # pylint: disable-next=global-variable-not-assigned
    global __BLINKING_ENABLED

    if enable is None or not __BLINKING_ENABLED or not __FORMAT_ENABLED:
        return ''

    if enable:
        return '\033[5m'

    return '\033[25m'


# pylint: disable-next=invalid-name,too-many-arguments
def get_format(reset=True, fg=None, fg_bright=False, bg=None, bg_bright=False,
               bold=None, faint=None, italic=None, underline=None, blink=None):
    """Escape sequence which will make a formatting. More information on the
    different formatting: https://en.wikipedia.org/wiki/ANSI_escape_code#Codes

    :param reset: Boolean to indicate to reset all previous format. Defaults to
                  True.
    :param fg: Foreground color. Color of the text printed. If None default
               foreground color from your terminal will be used. Defaults to
               None.
    :param fg_bright: Boolean to indicate to the bright version for the
                      foreground color. Defaults to False.
    :param bg: Background color. If None default background color from your
               terminal will be used. Defaults to None.
    :param bg_bright: Boolean to indicate to the bright version for the
                      background color. Defaults to False.
    :param bold: Boolean to indicate to the enable or disable the bold format
                 on the text. If None the bold format is left untouched. If
                 False it would also disable the faint formatting. Defaults to
                 None.
    :param faint: Boolean to indicate to the enable or disable the faint
                  format on the text. If None faint format is left untouched.
                  If False it would also disable the bold formatting. Defaults
                  to None.
    :param italic: Boolean to indicate to enable or disable the italic format
                   on the text. If None the italic format is left untouched.
                   Defaults to None.
    :param underline: Boolean to indicate to the enable or disable the
                      underline format on the text. If None underline format is
                      left untouched. Defaults to None.
    :param blink: Boolean to indicate to the enable or disable the slow blink
                  format on the text. If None blinking format is left
                  untouched. If False it would also disable the fast blink
                  formatting. Defaults to None.

    :return: Escape sequence which will produce the desired format on the text
             which follows it.
    """
    ret = get_color_format(reset=reset, fg=fg, fg_bright=fg_bright,
                           bg=bg, bg_bright=bg_bright, bold=bold,
                           faint=faint, italic=italic, underline=underline)

    ret += get_blink_format(blink)

    return ret


def format_string(*args, **kwargs):
    """Print a message which is formatted with the specified parameters.

    :param *args: Positional arguments to be printed. Like the built-in print()
                  method, each argument is printed separated by a space.
    :param **kwargs: Keyword arguments for the get_format() method.
    """
    if not args:
        return ''

    return (get_format(**kwargs) + ' '.join([str(x) for x in args]) +
            get_format())


def print_format(*args, reset=True, fg=None, fg_bright=None, bg=None,
                 bg_bright=False, bold=None, faint=None, italic=None,
                 underline=None, blink=None, **kwargs):
    """Print a message which is formatted with the specified parameters.

    :param *args: Positional arguments to be printed.
    :param reset: Boolean to indicate to reset all previous format. Defaults to
                  True.
    :param fg_bright: Boolean to indicate to the bright version for the
                      foreground color. Defaults to False.
    :param bg: Background color. If None default background color from your
               terminal will be used. Defaults to None.
    :param bg_bright: Boolean to indicate to the bright version for the
                      background color. Defaults to False.
    :param bold: Boolean to indicate to the enable or disable the bold format
                 on the text. If None the bold format is left untouched. If
                 False it would also disable the faint formatting. Defaults to
                 None.
    :param faint: Boolean to indicate to the enable or disable the faint format
                  on the text. If None faint format is left untouched. If False
                  it would also disable the bold formatting. Defaults to None.
    :param italic: Boolean to indicate to enable or disable the italic format
                   on the text. If None the italic format is left untouched.
                   Defaults to None.
    :param underline: Boolean to indicate to the enable or disable the
                      underline format on the text. If None underline format is
                      left untouched. Defaults to None.
    :param blink: Boolean to indicate to the enable or disable the slow blink
                  format on the text. If None blinking format is left
                  untouched. If False it would also disable the fast blink
                  formatting. Defaults to None.
    :param **kwargs: Additional key-word arguments which will be provided to
                     the builtin method print().
    """
    print(
        format_string(
            *args,
            reset=reset,
            fg=fg,
            fg_bright=fg_bright,
            bg=bg,
            bg_bright=bg_bright,
            bold=bold,
            faint=faint,
            italic=italic,
            underline=underline,
            blink=blink
        ),
        **kwargs
    )
