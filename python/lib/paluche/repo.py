"""Utils regarding git and svn repositories"""
import re
import os
import sys
import json
from enum import Enum


class RepoType(Enum):
    """Repository types"""
    GIT = 'git'
    SVN = 'svn'
    JJ = 'jj'
    JJ_GIT = 'jj git'
    ANY = 'jj git svn'

    def is_repo_root(self, path, main_only=False):
        """Find out if the provided path is a root of a repository.

        :param path: Path to check.
        :type path: str
        :param main_only: Do not consider sub-module as repository. Default to
                          False.

        :return: Type of the repository the path provided is the root, None if
                 we are not in a repository.
        :rtype: RepoType
        """
        is_ok = os.path.isdir if main_only else os.path.exists

        detected_value = ' '.join(
            value
            for value in self.value.split(' ')
            if is_ok(os.path.join(path, f'.{value}'))
        )

        full_value = ' '.join(
            value
            for value in self.ANY.value.split(' ')
            if is_ok(os.path.join(path, f'.{value}'))
        )

        if not detected_value:
            return None

        if self.value in (self.ANY.value, detected_value):
            return self.__class__(full_value)

        return None

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            other = self.__class__(other)

        self_values = self.value.split(' ')
        other_values = other.value.split(' ')

        if len(self_values) == len(other_values):
            return self_values == other_values

        def match(less_values, more_values):
            return all(value in more_values for value in less_values)

        if len(self_values) < len(other_values):
            return match(self_values, other_values)

        return match(other_values, self_values)


REMOTE_URL_RE = re.compile(
    r'^((?P<scheme>ssh|https|git?)://)?'
    r'((?P<user>[_\-\w]*)(:(?P<password>\w+))?@)?'
    r'((?P<host>[\-\w\.]+)[:/])?'
    # XXX: Missing PORT parsing.
    r'(?P<path>[\w\.\/-]*?)'
    r'(.git)?$'
)


# List of known remote and the pretty name associated.
REMOTE_HOSTS = {
    'github.com': 'github',
    'gitlab.com': 'gitlab',
    'git.kernel.org': 'kernel',
    'git.buildroot.net': '.',
    'bitbucket.org': 'bitbucket',
}

REMOTE_HOSTS_CONFIG = os.path.join(
    os.environ['HOME'], '.config', 'paluche', 'remote_hosts.json'
)


def load_remote_hosts():
    """Load the remote hosts dictionary."""
    ret = REMOTE_HOSTS.copy()

    if os.path.isfile(REMOTE_HOSTS_CONFIG):
        with open(
            REMOTE_HOSTS_CONFIG,
            mode='r',
            encoding='utf-8'
        ) as remote_hosts_file:
            ret.update(json.load(remote_hosts_file))

    return ret


def get_url_no_remote(repo_root):
    """Get URL information for a repository that would not have any remotes"""
    return {
        'scheme': None,
        'user': None,
        'host': None,
        'path': None,
        'host_pretty': None,
        'location': repo_root,
        'work_path': os.path.relpath(repo_root, os.environ['WORK_DIR']),
        'name': os.path.basename(repo_root),
    }


def parse_remote_url(url, remote_hosts):
    """Parse the URL of a git remote.

    :param url: URL of the remote to parse.
    :type url: str

    :return: Dictionary with the following keys:
                scheme: The scheme of the URL (ssh, http, https). None if no
                match.
                user: User name used to identify to the SSH host. None if no
                match.
                host: Host where the remote is located. None if no match.
                host_pretty: Pretty name for the host where the remote is
                             located. None if no match.
                path: Path of the git repository within the remote.
                work_path: Path where the git repository should be locally
                           cloned within the WORK_DIR.
                location: Absolute path where the git repository should locally
                          cloned within the WORK_DIR. Valid only if the
                          WORK_DIR environment variable is set.
                name: Name of the repository.

    :rtype: dict(str:str)
    """
    if match := REMOTE_URL_RE.match(url):
        ret = match.groupdict()
    else:
        ret = {
            'scheme': None,
            'user': None,
            'host': None,
            'path': url
        }

    ret['host_pretty'] = remote_hosts.get(ret['host'], None)
    ret['work_path'] = None
    ret['location'] = None

    if ret['host_pretty'] is not None:
        ret['work_path'] = os.path.relpath(
            os.path.join(ret['host_pretty'], ret['path'].replace('/', os.sep))
        )
        ret['location'] = os.path.join(os.environ.get('WORK_DIR', ''),
                                       ret['work_path'])
    elif ret['host']:
        print(
            f'Missing entry for host {ret["host"]}/{ret["path"]} in '
            f'REMOTE_HOST or {REMOTE_HOSTS_CONFIG}',
            file=sys.stderr
        )

    ret['name'] = os.path.basename(ret['path'])

    return ret


def get_parsed_url(remote_url, repo_root, remote_hosts):
    """Get the parsed URL associated with a repository. With support for local
    reposirory which would not have remotes set.
    """
    if remote_url is None:
        return get_url_no_remote(repo_root)

    return parse_remote_url(remote_url, remote_hosts)


def get_repo_root(current_dir, main_only=False, repo_type=RepoType.ANY):
    """Get the root of the repository we are in.

    :param current_dir: Path within the repository.
    :type current_dir: str
    :param main_only: Find the main repository root. Default to False.

    :return: The absolute path to the repository root, None if we are not in a
             repository.
    :rtype: str
    """
    current_dir = os.path.abspath(current_dir)
    repo_type = RepoType(repo_type)

    while True:
        if repo_type.is_repo_root(current_dir, main_only=main_only):
            return current_dir

        parent = os.path.dirname(current_dir)

        if current_dir == parent:
            # No home to get to.
            return None

        current_dir = parent


def search_for_repositories(search_path, repo_type=RepoType.ANY):
    """Search for main repositories within a directory"""
    if not isinstance(repo_type, RepoType):
        repo_type = RepoType(repo_type)
    ret = []

    for dir_path, dir_names, _ in os.walk(search_path):
        if res_repo_type := RepoType.ANY.is_repo_root(
            dir_path,
            main_only=True
        ):
            dir_names.clear()  # Do not walk deeper here.
            if res_repo_type == repo_type:
                ret.append(dir_path)

    return ret
