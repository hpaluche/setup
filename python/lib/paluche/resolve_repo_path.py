"""Common functions between resolve_repo_path and _resolve_repo_path-complete.
"""
import os
import paluche
import paluche.git
import paluche.jj
from paluche import repo


def add_option_args(parser):
    """Add common options between resolve_repo_path and
    _resolve_repo_path-complete.
    """
    parser.add_argument(
        '-m',
        '--main-only',
        action='store_true',
        help='Jumps to the local clone as main repository the specified '
             'argument points to.'
    )


def load_repositories(main_only=False):
    """Load the git repositories in the specified search directory."""
    remote_hosts = repo.load_remote_hosts()
    work_dir = os.environ['WORK_DIR']
    ret = (
        {}
        if main_only
        else paluche.git.load_current_repository(remote_hosts)
    )

    # Complete the list of repositories with the full list of repositories
    # available in the WORK_DIR.
    for repo_root in repo.search_for_repositories(
        work_dir,
        repo_type='jj git'
    ):
        for repo_type in ('git', 'jj'):
            if not os.path.isdir(os.path.join(repo_root, f'.{repo_type}')):
                continue

            url = getattr(paluche, repo_type).load_repo_url(
                repo_root,
                remote_hosts
            )
            if url['location'] in ret:
                break

            ret[url['location']] = {'url': url}
            break
        else:
            print(f'WTF {repo_root}')

    return {
        os.path.relpath(key, work_dir): value
        for key, value in ret.items()
    }


def get_repo_links(repositories):
    """Generate links from a the shorter name that could identify a repository
    to its full key.
    """
    conflicts = {}
    ret = {}

    for full_key in repositories:
        if not full_key:
            continue

        key = os.path.basename(full_key)

        if key in ret:
            if key in conflicts:
                conflicts[key].append(full_key)
            else:
                conflicts[key] = [ret[key], full_key]
                del ret[key]
        else:
            ret[key] = full_key

    for values in conflicts.values():
        common = os.path.commonpath(values)

        for value in values:
            ret[
                os.path.relpath(
                    value['url']['location'],
                    common
                )
            ] = value

    return ret
