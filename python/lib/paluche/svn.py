"""Utils regarding svn version control system"""
import os
import sys
from subprocess import check_output, DEVNULL
from enum import Enum
from paluche.repo import (
    search_for_repositories, RepoType, parse_remote_url, load_remote_hosts
)

# Custom svn ignores files location directory.
SVN_IGNORES_CONFIG = os.path.expanduser('~/.config/svn-status')


def get_svn_info(svn_root_dir):
    """Parse the output of the command `svn info`"""
    def parse_line(line):
        key, *value = line.split(': ', maxsplit=1)

        return (key, value[0] if value else None)

    return dict(
        map(
            parse_line,
            check_output(
                ['svn', 'info', svn_root_dir],
                encoding='utf-8',
                stderr=DEVNULL
            ).splitlines()
        )
    )


class SvnStatusState(Enum):
    """Enum for the state of a file in svn"""
    NONE = ' '
    ADDED = 'A'
    CONFLICTED = 'C'
    DELETED = 'D'
    IGNORED = 'I'
    MODIFIED = 'M'
    REPLACED = 'R'
    UNVERSIONED = 'X'
    UNTRACKED = '?'
    REMOVED = '!'
    OBSTRUCTED = '~'


class SvnStatusProperty(Enum):
    """Enum for the property status of a file in svn"""
    NONE = ' '
    CONFLICTED = 'C'
    MODIFIED = 'M'


class SvnStatusLocked(Enum):
    """Enum for the locked status of a file in svn"""
    NONE = ' '
    LOCKED = 'L'


class SvnStatusHistory(Enum):
    """Scheduled commit will create a copy (addition-with-history)"""
    NONE = ' '
    HISTORY = '+'


class SvnStatusSwitched(Enum):
    """Whether the item is switched or a file external"""
    NONE = ' '
    SWITCHED = 'S'
    EXTERNAL = 'X'


class SvnStatusRepositoryLocked(Enum):
    """Whether the item is locked in the repository for exclusive commit"""
    NONE = ' '
    LOCKED = 'K'
    LOCKED_BY_OTHER = 'O'
    LOCKED_STOLEN = 'T'
    LOCKED_BROKEN = 'B'


class SvnStatusConflict(Enum):
    """Whether the item is in conflict"""
    NONE = ' '
    CONFLICT = 'C'
    CONFLICT_REASON = '>'


class SvnStatusRemoteStatus(Enum):
    """Whether the item has remote changes"""
    NONE = ' '
    REMOTE_STATUS = '*'


def get_svn_status(directory, show_updates=False, verbose=False):
    """Parse the output of the command `svn status`"""
    class Rest():
        """Helper to parse the remaining text of the line"""
        def __init__(self, text):
            self.items = text.split(' ')
            self.index = 0

        def remaining(self):
            """Get remaining text"""
            # Update the index to the beginning of the remaining text
            next(self)
            return ' '.join(self.items[self.index - 1:])

        def __next__(self):
            while self.index < len(self.items):
                ret = self.items[self.index]
                self.index += 1

                if ret == '':
                    continue

                return ret

            return None

    def parse_line(line):
        # Common fields
        state = SvnStatusState(line[0])
        property_ = SvnStatusProperty(line[1])
        locked = SvnStatusLocked(line[2])
        history = SvnStatusHistory(line[3])
        switched = SvnStatusSwitched(line[4])
        repository_locked = SvnStatusRepositoryLocked(line[5])
        conflict = SvnStatusConflict(line[6])
        index = 7

        # Present only if show_updates is True
        if show_updates:
            remote_status = SvnStatusRemoteStatus(line[index])
            index += 1
        else:
            remote_status = None

        index += 1
        rest = Rest(line[index:])

        if (verbose or show_updates) and state != SvnStatusState.UNTRACKED:
            working_revision = next(rest)
        else:
            working_revision = None

        if verbose:
            working_copy_revision = next(rest)
            last_user = next(rest)
        else:
            working_copy_revision = None
            last_user = None

        file_path = rest.remaining()

        return (
            file_path,
            {
                'state': state,
                'property': property_,
                'locked': locked,
                'history': history,
                'switched': switched,
                'repository_locked': repository_locked,
                'conflict': conflict,
                'remote_status': remote_status,
                'working_revision': working_revision,
                'working_copy_revision': working_copy_revision,
                'last_user': last_user,
            }
        )

    cmd = ['svn', 'status', directory]

    if show_updates:
        cmd.append('--show-updates')

    if verbose:
        cmd.append('--verbose')

    status = [
        line
        for line in check_output(cmd, encoding='utf-8').splitlines()
        if (
            len(line.strip())
            and not line.startswith('Performing status on external item at')
            and not line.startswith('Status against revision:')
            and not line.startswith('Summary of conflicts:')
            and not line.startswith('  Tree conflicts:')
            and not line.startswith('  Text conflicts:')
        )
    ]

    return dict(map(parse_line, status))


def _list_refs(dir_path, dir_names, ref_dir):
    if ref_dir not in dir_names:
        return {}

    del dir_names[dir_names.index(ref_dir)]

    abs_ref_dir = os.path.join(dir_path, ref_dir)

    if ref_dir == 'trunk':
        return {
            'default': dir_path,
            'trunk': abs_ref_dir
        }

    ref_dirs = (
        os.path.join(abs_ref_dir, ref_dir)
        for ref_dir in os.listdir(abs_ref_dir)
    )

    return {
        os.path.basename(ref_dir): ref_dir
        for ref_dir in ref_dirs
        if os.path.isdir(ref_dir)
    }


def _load_refs(dir_path, dir_names):
    if not (ret := _list_refs(dir_path, dir_names, 'trunk')):
        return None

    for ref_dir in ('tags', 'branches'):
        ret.update(_list_refs(dir_path, dir_names, ref_dir))

    return ret


def load_refs(svn_root_dir):
    """Load the svn "references" available at the repository root."""
    dir_names = list(
        dir_name
        for dir_name in os.listdir(svn_root_dir)
        if os.path.isdir(os.path.join(svn_root_dir, dir_name))
    )

    return _load_refs(svn_root_dir, dir_names) or {'default': svn_root_dir}


def _list_sub_dirs(path):
    ret = []
    for dir_name in os.listdir(path):
        dir_path = os.path.join(path, dir_name)
        if (
            not os.path.isdir(dir_path)
            or dir_name in ('trunk', 'branches', 'tags')
        ):
            continue

        ret.append(dir_path)

    return ret


def search_for_sub_repo(svn_root_dir):
    """The idea of a sub-repository is a bit made up.
    We are actually looking for `trunk` directories which might be have tags/
    and branches/ associated
    """
    ret = {}

    for sub_dir_path in _list_sub_dirs(svn_root_dir):
        for dir_path, dir_names, _ in os.walk(sub_dir_path):
            if '.svn' in dir_names:
                del dir_names[dir_names.index('.svn')]

            if item := _load_refs(dir_path, dir_names):
                sub_repo_name = os.path.relpath(dir_path, svn_root_dir)

                ret[sub_repo_name] = item

                for sub_dir in dir_names:
                    sub_repo_root = os.path.join(dir_path, sub_dir)
                    for key, value in search_for_sub_repo(
                            sub_repo_root
                    ).items():
                        sub_repo_name = (
                            f'{sub_repo_name}:{os.path.join(sub_dir, key)}'
                        )
                        ret[sub_repo_name] = value

                # Do not walk deeper in this loop, it has been handled by u
                # recursive call to search_for_sub_repo().
                dir_names.clear()

    return ret


def _load_repo(path, remote_hosts):
    svn_info = get_svn_info(path)
    svn_info = parse_remote_url(svn_info['Repository Root'], remote_hosts)

    if svn_info['location'] and path != svn_info['location']:
        print(
            f'Unexpected location for the repository {svn_info["path"]}. '
            f'Actually in "{path}" should be in "{svn_info["location"]}".',
            file=sys.stderr
        )
    name = svn_info['name']
    ret = {
        name: {
            'url': svn_info,
            'refs': load_refs(path),
        }
    }

    ret.update(
        {
            f'{name}:{sub_name}': {
                'refs': sub_info
            }
            for sub_name, sub_info in search_for_sub_repo(path).items()
        }
    )

    return ret


def _reduce_keys(dict_):
    def reduce_name(name):
        names = name.split(':')

        return ':'.join(
            os.path.basename(name)
            for name in names
        )

    return dict(
        map(
            lambda name_info: (reduce_name(name_info[0]), name_info[1]),
            dict_.items()
        )
    )


def load_svn_repository(svn_root_dir):
    """Load the svn repository."""
    remote_hosts = load_remote_hosts()
    return _reduce_keys(_load_repo(svn_root_dir, remote_hosts))


def load_svn_repositories():
    """Load the svn repositories available in WORK_DIR."""
    ret = {}
    remote_hosts = load_remote_hosts()

    # Complete the list of repositories with the full list of repositories
    # available in the WORK_DIR.
    for path in search_for_repositories(
            os.environ['WORK_DIR'],
            repo_type=RepoType.SVN
    ):
        ret.update(_load_repo(path, remote_hosts))

    return _reduce_keys(ret)


def get_custom_ignores(svn_info, debug=None):
    """SVN does not have the .svnignore implemented. So I made this custom
    implementation. It will look for a file in SVN_IGNORES_CONFIG with the
    name of the repository UUID. If it exists, it will load the file and
    return the list of directories to ignore.

    The syntax is not the same as the .gitignore. It is a simple list of
    directories name to ignore. One per line. I do not need more for now. And
    probably never will need more.
    """
    if debug is None:
        def debug(*_args, **_kwargs):
            pass

    # Check if the selected repository has a custom ignore file.
    ignore_config_path = os.path.join(
        SVN_IGNORES_CONFIG,
        svn_info['Repository UUID']
    )

    ignores = []

    if os.path.exists(ignore_config_path):
        debug(f'Loading custom ignore file {ignore_config_path}')
        with open(ignore_config_path, mode='r', encoding='utf-8') as file:
            ignores = list(file.read().splitlines())
        debug(f'Loaded {", ".join(ignores)} ignores')
    else:
        debug(f'No custom ignore file found for {ignore_config_path}')

    return ignores
