"""Utils for ZSH autocompletion."""

import os


def __reduce_dir(target, dirs):
    ret = ''

    del dirs[dirs.index(target)]

    for char in target:
        ret += char
        if not any(dir_.startswith(ret) for dir_ in dirs):
            break

    return ret


def reduce_path(path, reduce_user=False, reduce_basename=False):
    """ZSH has a system of autocompletion where you can only write the
    beginning of the folder names for a path, then the autocompletion will
    resolve the path using TAB.

    The idea here is to take a long patch and reduce it in order ZSH can
    resolve it.
    """
    if reduce_user:
        home = os.environ.get('HOME')

        if home and path.startswith(home):
            path = '~' + path[len(home):]

    path = path.split(os.path.sep)
    full_path = path[0]
    ret = [path[0]]

    for element in (path[1:] if reduce_basename else path[1:-1]):
        full_path += os.sep + element

        if element in ('', '..', '.', '~'):
            ret.append(element)
            continue

        ret.append(
            __reduce_dir(
                element,
                os.listdir(
                    os.path.expanduser(
                        os.path.dirname(full_path)
                    )
                )
            )
        )

    if not reduce_basename:
        ret.append(path[-1])

    return os.sep.join(ret)
