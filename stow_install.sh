#! /usr/bin/env bash

THIS_SCRIPT=$(readlink -f "${0}")
SETUP_DIR="${THIS_SCRIPT%/*}"

#STOW="stow -R -n -v" # Dry run restow
#STOW="stow -v -R"
STOW="stow -R"

function stow_dotdir()
{
    name=$1
    target=${2}/${name//dot-/.}

    mkdir -p "${target}"

    ${STOW} "${name}" --dotfiles --no-folding --dir "${SETUP_DIR}" --target "${target}"
}

function stow_dotfile()
{
    name=$1
    target=$2

    ${STOW} "${name}" --dotfiles --dir "${SETUP_DIR}" --target "${target}"
}

# .config
stow_dotdir dot-config "${HOME}"

# .local
stow_dotdir dot-local "${HOME}"

# .zshrc
stow_dotfile zshrc "${HOME}"
