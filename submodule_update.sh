#!/usr/bin/env bash

THIS_SCRIPT=$(readlink -f "${0}")
SETUP_DIR="${THIS_SCRIPT%/*}"
ZSH_SYNTAX_HIGHLIGHTING_DIR=zsh-syntax-highlighting
SUBMODULE_PATH="${SETUP_DIR}/dot-config/zsh/${ZSH_SYNTAX_HIGHLIGHTING_DIR}"
PATCH_PATH="${SETUP_DIR}/resources/${ZSH_SYNTAX_HIGHLIGHTING_DIR}.patch"

usage()
{
  echo "usage $(basename "${THIS_SCRIPT}") [update|upgrade]"
}

if [ ${#} -ne 1 ]
then
  usage
  exit 2
fi

if [ "${1}" != "upgrade" ] && [ "${1}" != "update" ]
then
  usage
  exit 2
fi

git -C "${SETUP_DIR}" submodule sync

if [[ ! -d "${SUBMODULE_PATH}" ]]
then
  git -C "${SETUP_DIR}" submodule update --init
fi

git -C "${SUBMODULE_PATH}" reset --hard

if [ "${1}" == "upgrade" ]
then
  git -C "${SUBMODULE_PATH}" fetch
  git -C "${SUBMODULE_PATH}" checkout master
  git -C "${SUBMODULE_PATH}" rebase
elif [ "${1}" == "upgrade" ]
then
  git -C "${SETUP}" submodule update --init
fi

git -C "${SUBMODULE_PATH}" apply "${PATCH_PATH}"

if [ "${1}" == "upgrade" ]
then
  git -C "${SETUP}" add "${SUBMODULE_PATH}"
  git commit --message "${ZSH_SYNTAX_HIGHLIGHTING_DIR}: Update submodule"
fi
